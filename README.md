# TP2 - Bootstrap

**A rendre pour le 26/06/2020 au soir**

### Consignes :

Un client vous demande de créer la landing page de son extension de bookmark pour navigateur web. Cette page va premettre d'expliquer le fonctionnement de l'extension, de pouvoir la télécharger et de permettre de s'inscrire à la newsletter, avec un formulaire. Le client vous met à disposition les différentes maquettes graphique, avec les differents états (mobile), ainsi que les images à intégrer. Cette page devra être réalisée en HTML et en CSS. Vous devez déposer votre travail sur un dépot Gitlab public pour que votre client puisse s'occuper de sa mise en ligne.

### Points obligatoires :

* réaliser en HTML5, CSS3
* vous devez utiliser Bootstrap
* responsivité, respect des règles d'accessibilités